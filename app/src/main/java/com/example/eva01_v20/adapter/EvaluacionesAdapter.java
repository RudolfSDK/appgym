package com.example.eva01_v20.adapter;

import android.content.Context;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.eva01_v20.R;
import com.example.eva01_v20.data.entidades.Evaluacion;

import java.text.DecimalFormat;
import java.util.ArrayList;

public class EvaluacionesAdapter extends RecyclerView.Adapter<EvaluacionesAdapter.EvaluacionViewHolder> {
    private Context mContext;
    private Cursor mCursor;

    public EvaluacionesAdapter(Context context, Cursor cursor) {
        mContext = context;
        mCursor = cursor;
    }

    public static class EvaluacionViewHolder extends RecyclerView.ViewHolder{
        public ImageView mImageView;
        public TextView mTextView1;
        public TextView mTextView2;
        public TextView mTextView3;


        public EvaluacionViewHolder(@NonNull View itemView) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.itemImage);
            mTextView1 = itemView.findViewById(R.id.itemPeso);
            mTextView2 = itemView.findViewById(R.id.itemImc);
            mTextView3 = itemView.findViewById(R.id.itemFecha);
        }
    }

    @NonNull
    @Override
    public EvaluacionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.recycler_view_item,parent,false);

        return new EvaluacionViewHolder(view);
    }


    @Override
    public void onBindViewHolder(@NonNull EvaluacionViewHolder holder, int position) {
        if(!mCursor.moveToPosition(position)){
            return;
        }

        int peso = mCursor.getInt(1);
        float imc = mCursor.getFloat(2);
        int id = mCursor.getInt(0);
        String fecha = mCursor.getString(3);

        DecimalFormat decimalFormat = new DecimalFormat("#.#");

        String imcFinal = decimalFormat.format(imc);

        holder.mImageView.setImageResource(R.drawable.coach);
        holder.mTextView1.setText("Peso: "+String.valueOf(peso)+" Kg");
        holder.mTextView2.setText("IMC: "+imcFinal);
        holder.mTextView3.setText(fecha);
        holder.itemView.setTag(id);
    }

    @Override
    public int getItemCount() {
        return mCursor.getCount();
    }

    public void swapCursor(Cursor newCursor) {
        if (mCursor != null) {
            mCursor.close();
        }
        mCursor = newCursor;
        if (mCursor != null) {
            notifyDataSetChanged();
        }
    }
}
