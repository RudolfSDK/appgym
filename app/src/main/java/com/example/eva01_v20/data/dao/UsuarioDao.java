package com.example.eva01_v20.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.eva01_v20.data.ConexionSqLiteHelper;
import com.example.eva01_v20.data.entidades.Usuario;

import java.util.ArrayList;

public class UsuarioDao {
    Context context;
    ConexionSqLiteHelper conn;
    Usuario usuario;
    ArrayList<Usuario> usuarios;

    public UsuarioDao(Context context) {
        this.context = context;
    }

    public boolean insertarUsuario(Context context, Usuario usuario){
        boolean resultado= false;

        conn = new ConexionSqLiteHelper(context);
        SQLiteDatabase db = conn.getWritableDatabase();

        try{
            String insert = "INSERT INTO usuarios(username,nombre,apellido,fecha_nacimiento,estatura,clave)VALUES"+
                    "('"+usuario.getUserName()+"','"
                        +usuario.getNombre()+"','"
                        +usuario.getApellido()+"','"
                        +usuario.getFecha_nacimiento()+"','"
                        +usuario.getEstatura()+"','"
                        +usuario.getClave()+"')";
            db.execSQL(insert);

            resultado = true;

            db.close();

        }catch (Exception ex){
            Log.e("ErrorDaoUsuario",ex.toString());
            db.close();
        }finally {
            db.close();
        }


        return resultado;
    }

    public void updateUsuario(Context context,Usuario usuario){

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getWritableDatabase();

            String[] parametros = {String.valueOf(usuario.getId())};

            ContentValues values = new ContentValues();
            values.put("username",usuario.getUserName());
            values.put("nombre",usuario.getNombre());
            values.put("apellido",usuario.getApellido());
            values.put("fecha_nacimiento",usuario.getFecha_nacimiento());
            values.put("estatura",usuario.getEstatura());
            values.put("clave",usuario.getClave());

            db.update("usuarios",values,"id=?",parametros);

            conn.close();

        }catch (Exception ex){
            Log.e("ErrorDaoUsuario",ex.toString());
            conn.close();
        }
        finally {
            conn.close();
        }
    }

    public void deleteUsuario(Context context,Usuario usuario){

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getWritableDatabase();

            String[] parametros = {String.valueOf(usuario.getId())};

            db.delete("usuarios","id=?",parametros);

            conn.close();

        }catch (Exception ex){
            Log.e("ErrorDaoUsuario",ex.toString());
            conn.close();
        }
    }

    public ArrayList<Usuario> listarUsuarios(Context context){
        return usuarios;
    }

    public boolean login(Context context, String userName,String clave){
        boolean resultado;

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getReadableDatabase();

            String[] parametros = {userName,clave};
            String[] campos ={"id","estatura","username","clave"};

            Cursor cursor = db.query("usuarios",campos,"username=? AND clave=?",parametros,null,null,null);
            cursor.moveToFirst();
            int id = cursor.getInt(0);
            cursor.close();
            conn.close();
            resultado = true;

        }catch (Exception ex){
            Log.e("ErrorDaoUsuario",ex.toString());
            conn.close();
            resultado=false;
        }finally {
            conn.close();
        }

        return resultado;

    }
    public Usuario getUsuario(Context context, String userName){

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getReadableDatabase();

            String[] parametros = {userName};
            String[] campos ={"id","username","nombre","apellido","fecha_nacimiento","estatura","clave"};

            usuario = new Usuario();

            Cursor cursor = db.query("usuarios",campos,"username=?",parametros,null,null,null);
            cursor.moveToFirst();

            usuario.setId(cursor.getInt(0));
            usuario.setUserName(cursor.getString(1));
            usuario.setNombre(cursor.getString(2));
            usuario.setApellido(cursor.getString(3));
            usuario.setFecha_nacimiento(cursor.getString(4));
            usuario.setEstatura(cursor.getDouble(5));
            usuario.setClave(cursor.getString(6));

            cursor.close();
            conn.close();

        }catch (Exception ex){
            Log.e("ErrorDaoUsuario",ex.toString());
            conn.close();
        }finally {
            conn.close();
        }

        return usuario;

    }
}
