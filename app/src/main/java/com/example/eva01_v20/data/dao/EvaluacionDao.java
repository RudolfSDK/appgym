package com.example.eva01_v20.data.dao;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.eva01_v20.data.ConexionSqLiteHelper;
import com.example.eva01_v20.data.entidades.Evaluacion;

import java.util.ArrayList;

public class EvaluacionDao {
    Context context;
    ConexionSqLiteHelper conn;
    Evaluacion evaluacion;
    ArrayList<Evaluacion> evaluaciones;

    public EvaluacionDao(Context context) {
        this.context = context;
    }

    public boolean insertarEvaluacion(Evaluacion evaluacion){
        boolean resultado = false;

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getWritableDatabase();

            String insert = "INSERT INTO evaluaciones(peso,imc,fecha,id_usuario) VALUES"+
                        "('"+evaluacion.getPeso()+"','"
                            +evaluacion.getImc()+"','"
                            +evaluacion.getFecha()+"','"
                            +evaluacion.getId_usuario()+"')";

            db.execSQL(insert);

            resultado = true;

            conn.close();

        }catch (Exception ex){

            Log.e("ErrorEvaluacionDao",ex.toString());
            conn.close();

        }finally {

            conn.close();
        }


        return resultado;
    }

    public void updateEvaluacion(Evaluacion evaluacion){

        conn = new ConexionSqLiteHelper(context);

        try {
            SQLiteDatabase db = conn.getWritableDatabase();

            String[] parametros = {String.valueOf(evaluacion.getId())};

            ContentValues values = new ContentValues();
            values.put("peso",evaluacion.getPeso());
            values.put("imc",evaluacion.getImc());
            values.put("fecha",evaluacion.getFecha());
            values.put("id_usuario",evaluacion.getId_usuario());

            db.update("evaluaciones",values,"id=?",parametros);

            conn.close();

        }catch (Exception ex){
            Log.e("ErrorEvaluacionDao",ex.toString());
            conn.close();
        }
        finally {
            conn.close();
        }
    }

    public boolean deleteEvaluacion(int id){

        conn = new ConexionSqLiteHelper(context);

        try {

            SQLiteDatabase db = conn.getWritableDatabase();

            String[] parametros = {String.valueOf(id)};

            db.delete("evaluaciones","id=?",parametros);

            conn.close();

            return true;

        }catch (Exception ex){
            Log.e("ErrorEvaluacionDao",ex.toString());
            conn.close();
            return false;
        }
        finally {
            conn.close();
        }
    }

    public ArrayList<Evaluacion> getEvaluaciones(){

        conn = new ConexionSqLiteHelper(context);
        SQLiteDatabase db = conn.getReadableDatabase();
        evaluaciones = new ArrayList<Evaluacion>();

        Cursor cursor = db.rawQuery("SELECT * FROM evaluaciones",null);
        while (cursor.moveToNext()){
            evaluacion = new Evaluacion();
            evaluacion.setId(cursor.getInt(0));
            evaluacion.setPeso(cursor.getInt(1));
            evaluacion.setImc(cursor.getDouble(2));
            evaluacion.setFecha(cursor.getString(3));
            evaluacion.setId_usuario(cursor.getInt(4));

            evaluaciones.add(evaluacion);
        }

        return evaluaciones;
    }
}
