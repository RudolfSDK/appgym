package com.example.eva01_v20;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.EvaluacionDao;
import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Evaluacion;
import com.example.eva01_v20.data.entidades.Usuario;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.util.Calendar;

public class MainActivity_RegisterEV extends AppCompatActivity implements View.OnClickListener, CalendarView.OnDateChangeListener {

    CalendarView calendario;
    EditText peso;
    TextView imc;
    Button crear, calcular;
    String username;
    Usuario usuario;
    double pesoIngresado = 0;
    double imcCalculado;
    String fecha;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__register_e_v);

        username = getIntent().getExtras().getString("username");

        peso = findViewById(R.id.txtPeso);
        imc = findViewById(R.id.txtImc);
        calendario = findViewById(R.id.calendarView);
        calcular = findViewById(R.id.btnCalcular);
        calcular.setOnClickListener(this);
        crear = findViewById(R.id.btnCrear);
        crear.setOnClickListener(this);

        obtenerUsuario(username);

        calendario.setOnDateChangeListener(this);
    }

    private void obtenerUsuario(String username) {

        UsuarioDao usuarioDao = new UsuarioDao(getApplicationContext());

        usuario = usuarioDao.getUsuario(getApplicationContext(), username);

    }

    public void calcularIMC() {
        double estatura = usuario.getEstatura();
        pesoIngresado = Double.parseDouble(peso.getText().toString().trim());

        DecimalFormat decimalFormat = new DecimalFormat("#.#");

        imcCalculado = (pesoIngresado / Math.pow(estatura, 2));

        String imcFinal = decimalFormat.format(imcCalculado);

        imc.setText(imcFinal);
    }

    private void registrarEvaluacion() {

        EvaluacionDao evaluacionDao = new EvaluacionDao(getApplicationContext());

        Evaluacion evaluacion = new Evaluacion();
        evaluacion.setPeso(Integer.parseInt(peso.getText().toString().trim()));
        evaluacion.setImc(imcCalculado);
        evaluacion.setFecha(fecha);
        evaluacion.setId_usuario(usuario.getId());

        if (evaluacionDao.insertarEvaluacion(evaluacion)) {
            Toast.makeText(this, "Registro ingresado correctamente", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Registro con problemas", Toast.LENGTH_SHORT).show();
        }


    }

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnCalcular:
                if (validaCampos()) {
                    calcularIMC();
                }
                break;
            case R.id.btnCrear:
                if (validarIngreso()) {
                    registrarEvaluacion();
                }
                break;
        }

    }

    private boolean validarIngreso() {
        if(peso.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe ingresar su peso", Toast.LENGTH_SHORT).show();
            return false;
        }else if (imc.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe calcular su IMC", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validaCampos() {
        if (peso.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Debe ingresar su peso", Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }


    @Override
    public void onSelectedDayChange(@NonNull CalendarView calendarView, int year, int month, int dayOfMonth) {
        /*Calendar c = Calendar.getInstance();
        c.set(Calendar.YEAR, year);
        c.set(Calendar.MONTH, month);
        c.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        fecha = DateFormat.getDateInstance(DateFormat.MEDIUM).format(c.getTime());*/

        month+=1;
        fecha = String.valueOf(dayOfMonth).concat("/").concat(String.valueOf(month)).concat("/").concat(String.valueOf(year));
    }
}