package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity_Menu extends AppCompatActivity {

    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__menu);

        username = getIntent().getExtras().getString("username");
    }

    //Método para icono Evaluaciones
    public void Evaluaciones (View view){
        Intent evaluaciones = new Intent(this, MainActivity_MenuEV.class);
        evaluaciones.putExtra("username",username);
        startActivity(evaluaciones);
    }

}