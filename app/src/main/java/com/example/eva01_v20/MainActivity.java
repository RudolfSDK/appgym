package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.eva01_v20.data.dao.UsuarioDao;
import com.example.eva01_v20.data.entidades.Usuario;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    EditText userName;
    EditText password;
    Button login;
    Usuario usuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        userName = findViewById(R.id.txtUserName);
        password = findViewById(R.id.txtPasswordLogin);
        login = findViewById(R.id.buttoningresar);

        // Método en action Bar
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        getSupportActionBar().setIcon(R.mipmap.ic_launcher);

        login.setOnClickListener(this);

    }


    //Método Botón Registrarse
    public void Registrarse(View view){
            Intent registrarse = new Intent(this, MainActivity_Register.class);
            startActivity(registrarse);

    }



    private boolean validaCampos() {
        boolean valido = false;

        if (userName.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su nombre de usuario", Toast.LENGTH_SHORT).show();
        }else if(password.getText().toString().trim().isEmpty()){
            Toast.makeText(this, "Debe completar su contraseña", Toast.LENGTH_SHORT).show();
        }else {
            valido = true;
        }

        return valido;
    }

    //Método Botón login
    @Override
    public void onClick(View view) {

        if(validaCampos()){
            UsuarioDao usuarioDao = new UsuarioDao(this);

            if(usuarioDao.login(this,userName.getText().toString().trim(),password.getText().toString().trim())){

                Intent entrar = new Intent(this, MainActivity_Menu.class);
                entrar.putExtra("username",userName.getText().toString().trim());
                entrar.putExtra("pass",password.getText().toString().trim());

                startActivity(entrar);

            }else {

                Toast.makeText(this, "Revise sus credenciales o cree su usuario", Toast.LENGTH_SHORT).show();

            }
        }
    }
}

