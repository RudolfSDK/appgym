package com.example.eva01_v20;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class MainActivity_MenuEV extends AppCompatActivity {

    String username;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main__menu_e_v);

        username = getIntent().getExtras().getString("username");
    }

    //Método para ir a Crear Registro
    public void Crear (View view){
        Intent crear = new Intent(this, MainActivity_RegisterEV.class);
        crear.putExtra("username",username);
        startActivity(crear);
    }

    //Método para ir a Buscar Registro
    public void Buscar (View view){
        Intent buscar = new Intent(this, MainActivity_View.class);
        startActivity(buscar);
    }

    //Método para ir a Eliminar Registro
    public void Eliminar (View view){
        Intent eliminar = new Intent(this, MainActivity_DeleteEV.class);
        startActivity(eliminar);
    }
}